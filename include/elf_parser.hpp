// MIT License

// Copyright (c) 2018 finixbit
// Copyright (c) 2022 Colton Pawielski

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef H_ELF_PARSER
#define H_ELF_PARSER

#include <cstdio>
#include <cstdlib>
#include <elf.h> // Elf64_Shdr
#include <fcntl.h> /* O_RDONLY */
#include <fcntl.h>
#include <filesystem>
#include <iostream>
#include <memory>
#include <optional>
#include <string.h>
#include <string>
#include <sys/mman.h> /* mmap, MAP_PRIVATE */
#include <sys/stat.h> /* For the size of the file. , fstat */
#include <vector>

namespace elfparser {

class ElfSection {
protected:
    int m_Index, m_Size, m_EntSize, m_AddrAlign;
    std::intptr_t m_Offset, m_Address;
    std::string m_Name, m_Type;
    std::vector<char> m_Data;

public:
    ElfSection(const std::vector<char> elf, int index, std::string name, const Elf32_Shdr& shdr)
        : m_Index(index)
        , m_Size(shdr.sh_size)
        , m_EntSize(shdr.sh_entsize)
        , m_AddrAlign(shdr.sh_addralign)
        , m_Offset(shdr.sh_offset)
        , m_Address(shdr.sh_addr)
        , m_Name(name)
    {
        m_Data.resize(shdr.sh_size);
        memcpy(&m_Data[0], &elf[0] + shdr.sh_offset, shdr.sh_size);

        m_Type = [&shdr]() {
            switch (shdr.sh_type) {
                case 0: return "SHT_NULL"; /* Section header table entry unused */
                case 1: return "SHT_PROGBITS"; /* Program data */
                case 2: return "SHT_SYMTAB"; /* Symbol table */
                case 3: return "SHT_STRTAB"; /* String table */
                case 4: return "SHT_RELA"; /* Relocation entries with addends */
                case 5: return "SHT_HASH"; /* Symbol hash table */
                case 6: return "SHT_DYNAMIC"; /* Dynamic linking information */
                case 7: return "SHT_NOTE"; /* Notes */
                case 8: return "SHT_NOBITS"; /* Program space with no data (bss) */
                case 9: return "SHT_REL"; /* Relocation entries, no addends */
                case 11: return "SHT_DYNSYM"; /* Dynamic linker symbol table */
                default: return "UNKNOWN";
            }
        }();
    }

    int get_index() const { return m_Index; }
    std::string get_name() const { return m_Name; }
    std::string get_type() const { return m_Type; }
    std::intptr_t get_addr() const { return m_Address; }
    std::intptr_t get_offset() const { return m_Offset; }
    std::intptr_t get_size() const { return m_Size; }
    std::intptr_t get_entsize() const { return m_EntSize; }
    std::intptr_t get_alignment() const { return m_AddrAlign; }
    std::vector<char> data_ptr() const { return m_Data; };
};

class ElfSymbol {
protected:
    std::intptr_t m_Value;
    unsigned long m_Num;
    int m_Size;
    std::string m_Index, m_Type, m_Bind, m_Visability, m_Name, m_Section;
    std::vector<char> m_Data;

public:
    ElfSymbol(const std::vector<char> elf, unsigned long num, std::string symbolName, std::string sectionName, const Elf32_Sym& sym)
        : m_Value(sym.st_value)
        , m_Num(num)
        , m_Size(sym.st_size)
        , m_Index(get_symbol_index(sym.st_shndx))
        , m_Type(get_symbol_type(sym.st_info))
        , m_Bind(get_symbol_bind(sym.st_info))
        , m_Visability(get_symbol_visibility(sym.st_other))
        , m_Section(sectionName)
    {
        m_Data.resize(sym.st_size);
        memcpy(&m_Data[0], &elf[0] + sym.st_value, sym.st_size);
    }

    int get_num() const { return m_Num; }
    int get_size() const { return m_Size; }
    std::intptr_t get_value() const { return m_Value; }
    std::string get_type() const { return m_Type; }
    std::string get_bind() const { return m_Bind; }
    std::string get_visibility() const { return m_Visability; }
    std::string get_index() const { return m_Index; }
    std::string get_name() const { return m_Name; }
    std::string get_section() const { return m_Section; }
    std::vector<char> data_ptr() const { return m_Data; };

    std::string get_symbol_type(const uint8_t sym_type)
    {
        switch (ELF32_ST_TYPE(sym_type)) {
            case 0: return "NOTYPE";
            case 1: return "OBJECT";
            case 2: return "FUNC";
            case 3: return "SECTION";
            case 4: return "FILE";
            case 6: return "TLS";
            case 7: return "NUM";
            case 10: return "LOOS";
            case 12: return "HIOS";
            default: return "UNKNOWN";
        }
    }

    std::string get_symbol_bind(const uint8_t sym_bind)
    {
        switch (ELF32_ST_BIND(sym_bind)) {
            case 0: return "LOCAL";
            case 1: return "GLOBAL";
            case 2: return "WEAK";
            case 3: return "NUM";
            case 10: return "UNIQUE";
            case 12: return "HIOS";
            case 13: return "LOPROC";
            default: return "UNKNOWN";
        }
    }

    std::string get_symbol_visibility(const uint8_t sym_vis)
    {
        switch (ELF32_ST_VISIBILITY(sym_vis)) {
            case 0: return "DEFAULT";
            case 1: return "INTERNAL";
            case 2: return "HIDDEN";
            case 3: return "PROTECTED";
            default: return "UNKNOWN";
        }
    }

    std::string get_symbol_index(const uint16_t sym_idx)
    {
        switch (sym_idx) {
            case SHN_ABS: return "ABS";
            case SHN_COMMON: return "COM";
            case SHN_UNDEF: return "UND";
            case SHN_XINDEX: return "COM";
            default: return std::to_string(sym_idx);
        }
    }
};

class ElfSegment {
public:
    std::string segment_type, segment_flags;
    long segment_offset, segment_virtaddr, segment_physaddr, segment_filesize, segment_memsize;
    int segment_align;
};

class ElfRelocation {
public:
    std::intptr_t relocation_offset, relocation_info, relocation_symbol_value;
    std::string relocation_type, relocation_symbol_name, relocation_section_name;
    std::intptr_t relocation_plt_address;
};

class ElfParser {
private:
    const std::vector<char> m_elfData;

public:
    // Load ELF from std::vector<char>
    static std::optional<std::shared_ptr<ElfParser>> Load(const std::vector<char>& elfData);

    // Load ELF from Path
    static std::optional<std::shared_ptr<ElfParser>> Load(const std::filesystem::path& elfData);

    std::vector<ElfSection> get_sections();
    std::vector<ElfSegment> get_segments();
    std::vector<ElfSymbol> get_symbols();
    std::vector<ElfRelocation> get_relocations();

    std::vector<char> get_bin();

    std::vector<char> get_elf() { return m_elfData; };
    std::size_t get_elf_size() { return m_elfData.size(); };

protected:
    ElfParser(std::vector<char> elfData)
        : m_elfData(elfData) {};

    const char* get_base_address() { return &m_elfData[0]; }

    // template functions
    const Elf32_Ehdr* get_elf_header() { return (const Elf32_Ehdr*)get_base_address(); }

    static std::string get_segment_type(const uint32_t seg_type);
    static std::string get_segment_flags(const uint32_t seg_flags);
};

} // namespace elfparser
#endif