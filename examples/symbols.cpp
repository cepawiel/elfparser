#include <elf_parser.hpp>
#include <inttypes.h> // PRIx64
#include <iostream>

void print_symbols(std::vector<elfparser::ElfSymbol>& symbols);

int main(int argc, char* argv[])
{
    char usage_banner[] = "usage: ./sections [<executable>]\n";
    if (argc < 2) {
        std::cerr << usage_banner;
        return -1;
    }

    std::string program((std::string)argv[1]);
    auto elf_parser = elfparser::ElfParser::Load(program);
    if (!elf_parser.has_value()) {
        std::cerr << "Invalid ELF File" << std::endl;
        return -1;
    }

    auto syms = elf_parser.value()->get_symbols();
    print_symbols(syms);
    return 0;
}

void print_symbols(std::vector<elfparser::ElfSymbol>& symbols)
{
    printf("Num:    Value     Size Type     Bind    Vis       Ndx Name\n");
    for (auto& symbol : symbols) {
        printf("%-6d: %08" PRIx64 "  %-4d %-8s %-7s %-9s %-3s %s(%s)\n", symbol.get_num(), symbol.get_value(), symbol.get_size(), symbol.get_type().c_str(), symbol.get_bind().c_str(), symbol.get_visibility().c_str(), symbol.get_index().c_str(), symbol.get_name().c_str(), symbol.get_section().c_str());
    }
}