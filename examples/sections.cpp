#include <elf_parser.hpp>
#include <inttypes.h> // PRIx64
#include <iostream>

void print_sections(std::vector<elfparser::ElfSection>& sections);

int main(int argc, char* argv[])
{
    char usage_banner[] = "usage: ./sections [<executable>]\n";
    if (argc < 2) {
        std::cerr << usage_banner;
        return -1;
    }

    std::string program((std::string)argv[1]);
    auto elf_parser = elfparser::ElfParser::Load(program);
    if (!elf_parser.has_value()) {
        std::cerr << "Invalid ELF File" << std::endl;
        return -1;
    }
    auto secs = elf_parser.value()->get_sections();
    print_sections(secs);

    return 0;
}

void print_sections(std::vector<elfparser::ElfSection>& sections)
{
    printf("  [Nr] %-16s %-16s %-16s %-8s", "Name", "Type", "Address", "Offset");
    printf(" %-16s %-16s %5s\n", "Size", "EntSize", "Align");

    for (auto& section : sections) {
        printf("  [%2d] %-16s %-16s %016" PRIx64 " %08" PRIx64, section.get_index(), section.get_name().c_str(), section.get_type().c_str(), section.get_addr(), section.get_offset());

        printf(" %016zx %016" PRIx64 " %5" PRIu64 "\n", section.get_size(), section.get_entsize(), section.get_alignment());
    }
}