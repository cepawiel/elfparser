// MIT License

// Copyright (c) 2018 finixbit
// Copyright (c) 2022 Colton Pawielski

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "elf_parser.hpp"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iterator>

using namespace elfparser;

std::vector<ElfSection> ElfParser::get_sections()
{
    const Elf32_Ehdr* ehdr = get_elf_header();
    const Elf32_Shdr* shdr = (Elf32_Shdr*)(get_base_address() + ehdr->e_shoff);
    int shnum = ehdr->e_shnum;

    const Elf32_Shdr* sh_strtab = &shdr[ehdr->e_shstrndx];
    const char* const sh_strtab_p = get_base_address() + sh_strtab->sh_offset;

    std::vector<ElfSection> sections;
    for (int i = 0; i < shnum; ++i) {
        ElfSection section(m_elfData, i, std::string(sh_strtab_p + shdr[i].sh_name), shdr[i]);
        sections.push_back(section);
    }
    return sections;
}

std::vector<ElfSegment> ElfParser::get_segments()
{
    const Elf32_Ehdr* ehdr = get_elf_header();
    const Elf32_Phdr* phdr = (const Elf32_Phdr*)(get_base_address() + ehdr->e_phoff);
    int phnum = ehdr->e_phnum;

    std::vector<ElfSegment> segments;
    for (int i = 0; i < phnum; ++i) {
        ElfSegment segment;
        segment.segment_type = get_segment_type(phdr[i].p_type);
        segment.segment_offset = phdr[i].p_offset;
        segment.segment_virtaddr = phdr[i].p_vaddr;
        segment.segment_physaddr = phdr[i].p_paddr;
        segment.segment_filesize = phdr[i].p_filesz;
        segment.segment_memsize = phdr[i].p_memsz;
        segment.segment_flags = get_segment_flags(phdr[i].p_flags);
        segment.segment_align = phdr[i].p_align;

        segments.push_back(segment);
    }
    return segments;
}

std::vector<ElfSymbol> ElfParser::get_symbols()
{
    auto secs = get_sections();

    // get strtab
    const char* sh_strtab_p = nullptr;
    for (auto& sec : secs) {
        if ((sec.get_type() == "SHT_STRTAB") && (sec.get_name() == ".strtab")) {
            sh_strtab_p = get_base_address() + sec.get_offset();
            break;
        }
    }

    // get dynstr
    const char* sh_dynstr_p = nullptr;
    for (auto& sec : secs) {
        if ((sec.get_type() == "SHT_STRTAB") && (sec.get_name() == ".dynstr")) {
            sh_dynstr_p = get_base_address() + sec.get_offset();
            break;
        }
    }

    std::vector<ElfSymbol> symbols;
    for (auto& sec : secs) {
        if ((sec.get_type() != "SHT_SYMTAB") && (sec.get_type() != "SHT_DYNSYM"))
            continue;

        auto total_syms = sec.get_size() / sizeof(Elf32_Sym);
        auto syms_data = (Elf32_Sym*)(get_base_address() + sec.get_offset());

        for (unsigned long i = 0; i < total_syms; ++i) {
            std::string name = "";
            if (sec.get_type() == "SHT_SYMTAB")
                name = std::string(sh_strtab_p + syms_data[i].st_name);

            if (sec.get_type() == "SHT_DYNSYM")
                name = std::string(sh_dynstr_p + syms_data[i].st_name);

            ElfSymbol symbol(m_elfData, i, name, sec.get_name(), syms_data[i]);
            symbols.push_back(symbol);
        }
    }
    return symbols;
}

std::vector<char> ElfParser::get_bin()
{
    auto segments = get_segments();
    auto sections = get_sections();

    std::intptr_t min_load_addr = INTPTR_MAX, max_load_addr = INTPTR_MIN;
    for (const elfparser::ElfSegment& segment : segments) {
        if (segment.segment_type != "LOAD")
            continue;

        if (min_load_addr > segment.segment_physaddr) {
            min_load_addr = segment.segment_physaddr;
        }

        auto seg_end = segment.segment_physaddr + segment.segment_filesize;
        if (max_load_addr < seg_end) {
            max_load_addr = seg_end;
        }
    }
    assert(max_load_addr > min_load_addr);

    auto end = std::remove_if(sections.begin(), sections.end(), [min_load_addr, max_load_addr](const elfparser::ElfSection& section) { return (section.get_addr() < min_load_addr) || (section.get_addr() > max_load_addr); });
    sections.erase(end, sections.end());

    // unsure if elf always has sections ordered, sorting to be sure
    std::sort(sections.begin(), sections.end(), [](const elfparser::ElfSection& a, const elfparser::ElfSection& b) { return a.get_addr() < b.get_addr(); });

    // build bin
    auto bin_size = max_load_addr - min_load_addr;
    std::vector<char> bin_data(bin_size);
    for (elfparser::ElfSection section : sections) {
        auto offset = section.get_addr() - min_load_addr;
        memcpy(&bin_data[offset], &section.data_ptr()[0], section.get_size());
    }
    return bin_data;
}

std::string ElfParser::get_segment_type(const uint32_t seg_type)
{
    switch (seg_type) {
        case PT_NULL: return "NULL"; /* Program header table entry unused */
        case PT_LOAD: return "LOAD"; /* Loadable program segment */
        case PT_DYNAMIC: return "DYNAMIC"; /* Dynamic linking information */
        case PT_INTERP: return "INTERP"; /* Program interpreter */
        case PT_NOTE: return "NOTE"; /* Auxiliary information */
        case PT_SHLIB: return "SHLIB"; /* Reserved */
        case PT_PHDR: return "PHDR"; /* Entry for header table itself */
        case PT_TLS: return "TLS"; /* Thread-local storage segment */
        case PT_NUM: return "NUM"; /* Number of defined types */
        case PT_LOOS: return "LOOS"; /* Start of OS-specific */
        case PT_GNU_EH_FRAME: return "GNU_EH_FRAME"; /* GCC .eh_frame_hdr segment */
        case PT_GNU_STACK: return "GNU_STACK"; /* Indicates stack executability */
        case PT_GNU_RELRO:
            return "GNU_RELRO"; /* Read-only after relocation */
        // case PT_LOSUNW: return "LOSUNW";
        case PT_SUNWBSS: return "SUNWBSS"; /* Sun Specific segment */
        case PT_SUNWSTACK:
            return "SUNWSTACK"; /* Stack segment */
        // case PT_HISUNW: return "HISUNW";
        case PT_HIOS: return "HIOS"; /* End of OS-specific */
        case PT_LOPROC: return "LOPROC"; /* Start of processor-specific */
        case PT_HIPROC: return "HIPROC"; /* End of processor-specific */
        default: return "UNKNOWN";
    }
}

std::string ElfParser::get_segment_flags(const uint32_t seg_flags)
{
    std::string flags;

    if (seg_flags & PF_R)
        flags.append("R");

    if (seg_flags & PF_W)
        flags.append("W");

    if (seg_flags & PF_X)
        flags.append("E");

    return flags;
}

std::optional<std::shared_ptr<ElfParser>> ElfParser::Load(const std::vector<char>& elfData)
{
    const Elf32_Ehdr* header = (const Elf32_Ehdr*)&elfData[0];
    switch (header->e_ident[EI_CLASS]) {
        case ELFCLASS32: return std::shared_ptr<ElfParser>(new ElfParser(elfData));
        default: std::cout << "Unsupported ELF" << std::endl; return {};
    };
}

std::optional<std::shared_ptr<ElfParser>> ElfParser::Load(const std::filesystem::path& elfPath)
{
    auto size = static_cast<int32_t>(std::filesystem::file_size(elfPath));
    std::ifstream stream;
    stream.open(elfPath, std::ios::binary);
    if (!stream.is_open()) {
        std::cerr << "Failed to open elf at " << elfPath << std::endl;
        return {};
    }
    std::vector<char> elf_data(size);
    stream.read(&elf_data[0], size);
    return Load(elf_data);
}